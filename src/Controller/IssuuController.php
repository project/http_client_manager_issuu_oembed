<?php

namespace Drupal\http_client_manager_issuu_oembed\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;
use Drupal\http_client_manager\Entity\HttpConfigRequest;
use Drupal\http_client_manager\HttpClientInterface;
use Drupal\http_client_manager\HttpServiceApiWrapperFactoryInterface;
use Drupal\http_client_manager\Plugin\HttpServiceApiWrapper\HttpServiceApiWrapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IssuuController.
 *
 * @package Drupal\http_client_manager_issuu_oembed\Controller
 */
class IssuuController extends ControllerBase {

  /**
   * JsonPlaceholder Http Client.
   *
   * @var \Drupal\http_client_manager\HttpClientInterface
   */
  protected $httpClient;

  /**
   * The Issuu Api Wrapper service.
   *
   * @var \Drupal\http_client_manager_issuu_oembed\Plugin\HttpServiceApiWrapper\HttpServiceApiWrapperIssuu
   */
  protected $api;

  /**
   * The HTTP Service Api Wrapper Factory service.
   *
   * @var \Drupal\http_client_manager\HttpServiceApiWrapperFactoryInterface
   */
  protected $apiFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(HttpClientInterface $http_client, HttpServiceApiWrapperInterface $api_wrapper, HttpServiceApiWrapperFactoryInterface $api_wrapper_factory) {
    $this->httpClient = $http_client;
    $this->api = $api_wrapper;
    $this->apiFactory = $api_wrapper_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('issuu_oembed_api.http_client'),
      $container->get('http_client_manager_issuu_oembed.api_wrapper.issuu'),
      $container->get('http_client_manager.api_wrapper.factory')
    );
  }

  /**
   * Get Client.
   *
   * @return \Drupal\http_client_manager\HttpClientInterface
   *   The Http Client instance.
   */
  public function getClient() {
    return $this->httpClient;
  }

  /**
   * Get Issuu - by URL.
   *
   * @param string|null $url
   *   The Issuu URL.
   *
   * @return array
   *   A render array of the Issuu Oembed response.
   */
  public function getIssuu($url = NULL) {
    $build = [];
    $request = \Drupal::request();
    $url = $url ?? $request->query->get('url');
    if (!empty($url)) {
      $show_thumbnails = ($request->query->get('show_thumbnails') == 'true') ? TRUE: FALSE;
      $show_heading = ($request->query->get('show_heading') == 'false') ? FALSE: TRUE;
      $show_description = ($request->query->get('show_description') == 'false') ? FALSE: TRUE;
      $response = $this->api->getIssuuOembed($url);
      $response = [$url => $response];
      foreach ($response as $id => $resp) {
        $build[$id] = $this->buildIssuuResponse($resp, $show_thumbnails, $show_heading, $show_description);
      }
      return $build;
    } else {
      return $build;
    }
  }

  /**
   * Get Issuu - by Autor Document.
   *
   * @param string|null $author_name
   *   The autor name.
   * @param string|null $document_name
   *   The document name.
   *
   * @return array
   *   A render array of the Issuu Oembed response.
   */
  public function getIssuuAuthorDocument($author_name = NULL, $document_name = NULL) {
    $build = [];
    if (!empty($author_name) and !empty($document_name)) {
      $request = \Drupal::request();
      $show_thumbnails = ($request->query->get('show_thumbnails') == 'true') ? TRUE: FALSE;
      $show_heading = ($request->query->get('show_heading') == 'true') ? TRUE: FALSE;
      $show_description = ($request->query->get('show_description') == 'false') ? FALSE: TRUE;
      $url = "https://issuu.com/{$author_name}/docs/{$document_name}";
      $response = $this->api->getIssuuOembed($url);
      $response = [$url => $response];
      foreach ($response as $id => $resp) {
        $build[$id] = $this->buildIssuuResponse($resp, $show_thumbnails, $show_heading, $show_description);
      }
      return $build;
    } else {
      return $build;
    }
  }

  /**
   * Get Issuu - Title.
   *
   * @param string|null $author_name
   *   The author name.
   * @param string|null $document_name
   *   The document name.
   *
   * @return string
   *   The Issuu title.
   */
  public function getTitleAuthorDocument($author_name = NULL, $document_name = NULL) {
    if (!empty($author_name) and !empty($document_name)) {
      $url = "https://issuu.com/{$author_name}/docs/{$document_name}";
      $response = $this->api->getIssuuOembed($url);
      return $response['title'];
    }
    else {
      return 'Issuu';
    }
  }

  /**
   * Build Issuu Response.
   *
   * @param array $resp
   *   The Issuu response item.
   * @param bool $show_thumbnails
   *   TRUE to show Thumbnails.
   * @param bool $show_heading
   *   TRUE to show Issute title in Heading.
   * @param bool $show_description
   *   TRUE to show Description.
   *
   * @return array
   *   A render array of the Issuu Oembed response.
   */
  protected function buildIssuuResponse(array $resp, $show_thumbnails, $show_heading, $show_description) {
    $output = [];
    $url = $resp['url'];
    $title = $resp['title'];
    if ($show_heading) {
      $output['heading'] = [
        '#markup' => "<h2><a href='{$url}'>{$title}</a></h2>",
      ];
    }
    if ($show_description) {
      $output['description'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $resp['description'],
      ];
    }
    if (!empty($resp['html'])) {
      foreach (Html::load($resp['html'])->getElementsByTagName('iframe') as $iframe) {
        $iframe_src = $iframe->getAttribute('src');
      }
      if (!empty($iframe_src)) {
        $iframe_height = $resp['height'];
        $iframe_width = $resp['width'];
        $output['iframe'] = [
          '#type' => 'html_tag',
          '#tag' => 'iframe',
          '#attributes' => [
            'src' => $iframe_src,
            'allow' => 'fullscreen',
            'loading' => 'lazy',
            'height' => $iframe_height,
          ],
        ];
      } else {
        $output['iframe'] = [
          '#type' => 'inline_template',
          '#template' => $iframe_html,
        ];
      }
    }
    if ($show_thumbnails) {
      $thumbnail_url_medium = $resp['thumbnail_url'];
      $thumbnail_url_large = str_replace('thumb_medium', 'thumb_large', $thumbnail_url_medium);
      $thumbnail_url_small = str_replace('thumb_medium', 'thumb_small', $thumbnail_url_medium);
      $output['thumbnails'] = [
        '#type' => 'html_tag',
        '#tag' => 'details',
        'summary' => [
          '#type' => 'html_tag',
          '#tag' => 'summary',
          '#value' => "Thumbnails",
        ],
        'thumb_small' => [
          '#markup' => "<a href='{$url}'><img src='{$thumbnail_url_small}' alt='{$title}'></a>",
        ],
        'thumb_medium' => [
          '#markup' => "<a href='{$url}'><img src='{$thumbnail_url_medium}' alt='{$title}'></a>",
        ],
        'thumb_large' => [
          '#markup' => "<a href='{$url}'><img src='{$thumbnail_url_large}' alt='{$title}'></a>",
        ],
      ];
    }

    return $output;
  }

  /**
   * Check Token module.
   */
  protected function checkTokenModule() {
    if (!$this->moduleHandler()->moduleExists('token')) {
      $message = $this->t('Install the Token module in order to use tokens inside your HTTP Config Requests.');
      \Drupal::messenger()->addWarning($message);
    }
  }

}
