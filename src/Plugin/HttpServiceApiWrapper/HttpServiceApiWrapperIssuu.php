<?php

namespace Drupal\http_client_manager_issuu_oembed\Plugin\HttpServiceApiWrapper;

use Drupal\http_client_manager\Plugin\HttpServiceApiWrapper\HttpServiceApiWrapperBase;
use Drupal\http_client_manager_issuu_oembed\api\Commands\Issuu;

/**
 * Class HttpServiceApiWrapperIssuu.
 *
 * @package Drupal\http_client_manager\Plugin\HttpServiceApiWrapper
 */
class HttpServiceApiWrapperIssuu extends HttpServiceApiWrapperBase {

  /**
   * {@inheritdoc}
   */
  public function getHttpClient() {
    return $this->httpClientFactory->get('issuu_oembed_services');
  }

  /**
   * Get Issuu.
   *
   * @param string $url
   *   The Issuu url.
   *
   * @return array
   *   An array representing the Issuu matching the provided url.
   */
  public function GetIssuuOembed($url) {
    $args = [
      'format' => (string) 'json',
      'url' => (string) $url,
    ];
    return $this->call(Issuu::GET_ISSUU_OEMBED, $args)->toArray();
  }

}
