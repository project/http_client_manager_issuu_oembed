<?php

namespace Drupal\http_client_manager_issuu_oembed\EventSubscriber;

use Drupal\http_client_manager\Event\HttpClientEvents;
use Drupal\http_client_manager\Event\HttpClientHandlerStackEvent;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class HttpClientManagerIssuuSubscriber.
 */
class HttpClientManagerIssuuSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HttpClientEvents::HANDLER_STACK => ['onHandlerStack'],
    ];
  }

  /**
   * This method is called whenever the http_client.handler_stack event is
   * dispatched.
   *
   * @param \Drupal\http_client_manager\Event\HttpClientHandlerStackEvent $event
   *   The HTTP Client Handler stack event.
   */
  public function onHandlerStack(HttpClientHandlerStackEvent $event) {
    if ($event->getHttpServiceApi() != 'issuu_oembed_services') {
      return;
    }

    $handler = $event->getHandlerStack();
    $middleware = Middleware::mapRequest([$this, 'addIssuuOembedServiceHttpHeader']);
    $handler->push($middleware, 'issuu_oembed_services');
  }

  /**
   * Add Issuu Oembed service HTTP Header.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The current Request object.
   *
   * @return \Psr\Http\Message\MessageInterface
   *   Return an instance with the provided value for the specified header.
   */
  public function addIssuuOembedServiceHttpHeader(RequestInterface $request) {
    return $request->withHeader('X-ISSUU-HTTP-HEADER', 'issuu_oembed_services');
  }

}
