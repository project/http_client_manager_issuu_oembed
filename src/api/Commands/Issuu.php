<?php

namespace Drupal\http_client_manager_issuu_oembed\api\Commands;

/**
 * Class Issuu.
 *
 * Contains all the Guzzle Commands defined inside the "issuu" Guzzle Service
 * Description.
 *
 * @package Drupal\http_client_manager_issuu_oembed\api\Commands
 */
final class Issuu {

  const GET_ISSUU_OEMBED = 'GetIssuuOembed';

}
